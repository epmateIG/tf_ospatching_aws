variable "ami" {
  type = string
  default = "ami-001ab359d86bd04ed"
}

variable "instance_type" {
  type = string
  default = "t3.micro"
}

variable "key_pair" {
  type = string
  default = ""
}

variable "subnet_id" {
  type = string
  default = "subnet-39de8b7f"
}

variable "keypair" {
  type = string
  default = "c8ytestSingapore"
}

variable "vpc_ids" {
  type = string
  default = "vpc-ae099dcb"
}

variable "set" {
  type = string
  default = "t1"
}

variable "kms_key" {
  type = string
  default = "arn:aws:kms:ap-southeast-1:358698694027:key/90624d6d-2e79-431f-bd91-03630fc9cf20"
}