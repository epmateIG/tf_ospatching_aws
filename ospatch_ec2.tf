provider "aws" {
    region = "ap-southeast-1"
}

# Create EC2 Instance
resource "aws_instance" "os-dev" {
  ami                         = var.ami
  instance_type               = var.instance_type
  subnet_id                   = var.subnet_id
  #vpc_ids                     = var.vpc_ids
  vpc_security_group_ids      = [aws_security_group.os-sg.id]
  associate_public_ip_address = true
  source_dest_check           = false
  key_name                    = var.keypair
  user_data                   = file("aws-user-data-dev.sh")
  
  # root disk
  root_block_device {
    volume_size           = "20"
    volume_type           = "gp2"
    encrypted             = true
    delete_on_termination = true
    kms_key_id            = var.kms_key
    #kms_key_id            = aws_kms_key.sg-kms-key.key_id
  }
  
  tags = {
    Name        = "${var.set}-ospatching-dev"
    Environment = "dev"
    UserId       = "mohdshahmi.abdulmajid@softwareag.com"
  }
}

# Define the security group for HTTP web server
resource "aws_security_group" "os-sg" {
  name = "ospatching-sg"
  description = "ospatching-sg"
  vpc_id = var.vpc_ids

  ingress {
    from_port = 0
    to_port = 0
    protocol = "-1"
    cidr_blocks = ["172.31.0.0/16"]
  }

  ingress {
    from_port = 22
    to_port = 22
    protocol = "tcp"
    cidr_blocks = ["18.194.26.243/32"]
   }

  egress {
    from_port = 0
    to_port = 0
    protocol = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    Name = "ospatching-sg"
    #Environment = var.app_environment
  }
}