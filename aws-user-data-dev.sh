#! /bin/bash
sudo hostnamectl set-hostname t1-ospatching-dev.c8y.io
privateip=$(ip addr | grep 'state UP' -A2 | tail -n1 | awk '{print $2}' | cut -f1  -d'/')
echo "$privateip $(hostname -f)" | sudo tee -a /etc/hosts
sudo yum install -y vim telnet
sudo curl -sS --insecure 'https://foreman.c8y.io:9090/register?force=true&hostgroup_id=12&lifecycle_environment_id=3&location_id=7&operatingsystem_id=2&organization_id=3&setup_insights=false&setup_remote_execution=true' -H 'Authorization: Bearer eyJhbGciOiJIUzI1NiJ9.eyJ1c2VyX2lkIjo0LCJpYXQiOjE2MzA5ODk1NTEsImp0aSI6Ijc4OTVmZjU0MmQ5MzQ0ZTcyYzMyZjljZTJjZWMzYmFlZDdhNTBiYzlhYjcwZWI4MGQ0YmY0MDU4NWI5OWIyNzQiLCJzY29wZSI6InJlZ2lzdHJhdGlvbiNnbG9iYWwgcmVnaXN0cmF0aW9uI2hvc3QifQ.bEY6yjLRHvmqx5HEDMd8KJnxh0C1UZ97Sz8inS1_b3s' | bash