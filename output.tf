output "dev_server_instance_private_ip" {
  value = aws_instance.os-dev.private_ip
}

output "dev_server_instance_public_ip" {
  value = aws_instance.os-dev.public_ip
}