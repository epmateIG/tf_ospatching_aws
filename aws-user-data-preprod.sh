#! /bin/bash
sudo hostnamectl set-hostname t1-ospatching-preprod.c8y.io.c8y.io
privateip=$(ip addr | grep 'state UP' -A2 | tail -n1 | awk '{print $2}' | cut -f1  -d'/')
echo "$privateip $(hostname -f)" | sudo tee -a /etc/hosts
sudo yum install -y vim telnet